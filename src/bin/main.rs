use vector;
use vector::Vector3;

use std::rc::Rc;

use ray_tracer::color;
use ray_tracer::ray;
use ray_tracer::sphere::Sphere;
use ray_tracer::HittableList;
use ray_tracer::camera::Camera;
use ray_tracer::material::{Material, Lambertian, Metal, Dielectric};

use rand::Rng;

fn ray_color(r: &ray::Ray, world: &HittableList, depth: i32) -> color::Color {


    // if we've exceeded the ray bounce limit, no more light is gathered
    if depth <= 0 {
        return vector::Vector3::new(0.0, 0.0, 0.0);
    }

    // Hit
    // ---
    if let Some(record) = world.hit(r, 0.001, f64::INFINITY) {
        //let target = record.p + record.normal + vector::Vector3::random_unit_vector();
        //let target = record.p + vector::Vector3::random_in_hemisphere(&record.normal);
        if let Some( (scattered, attenuation) ) = record.mat_ptr.scatter(&r, &record) {
            return ray_color(&scattered, world, depth-1) * attenuation;
        } else {
            return vector::Vector3::new(0.0, 0.0, 0.0);
        }
    }

    // No hit
    // ------
    let unit_direction = vector::Vector3::unit_vector(&r.direction);
    let t = 0.5 * (unit_direction.y + 1.0);

    let white = vector::Vector3::new(1.0,1.0,1.0) as color::Color;
    let blue = vector::Vector3::new(0.5, 0.7, 1.0) as color::Color;

    let color_acquired = white * (1.0-t) + blue * t;

    color_acquired
}

fn random_scene() -> HittableList {
    let mut world = HittableList::new();

    let ground_material: Rc<dyn Material> = Rc::new(Lambertian::new(Vector3::new(0.5,0.5,0.5)));
    world.add(Rc::new(Sphere::new(Vector3::new(0.0,-1000.0,0.0),1000.0, Rc::clone(&ground_material))));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = rand::thread_rng().gen_range(0.0,1.0);
            let center = Vector3::new((a as f64) + 0.9*rand::thread_rng().gen_range(0.0,1.0), 
                                      0.2,
                                      (b as f64) + 0.9 * rand::thread_rng().gen_range(0.0,1.0)
                                      );
            if (center - Vector3::new(4.0, 0.2, 0.0)).len() > 0.9 {
                if choose_mat < 0.8 {
                    // diffuse
                    let albedo = Vector3::random() * Vector3::random();
                    let mat = Lambertian::new(albedo);
                    let sphere_material: Rc<dyn Material> = Rc::new(mat);
                    world.add(Rc::new(Sphere::new(center,0.2, Rc::clone(&sphere_material))));
                } else if choose_mat < 0.95 {
                    //metal
                    let albedo = Vector3::random();
                    let fuzz = rand::thread_rng().gen_range(0.0,1.0);
                    let mat = Metal::new(albedo, fuzz);
                    let sphere_material: Rc<dyn Material> = Rc::new(mat);
                    world.add(Rc::new(Sphere::new(center, 0.2, Rc::clone(&sphere_material))));
                } else {
                    //glass
                    let mat = Dielectric::new(1.5);
                    let sphere_material: Rc<dyn Material> = Rc::new(mat);
                    world.add(Rc::new(Sphere::new(center,0.2,Rc::clone(&sphere_material))));
                }
            }
        }
    }

    let material1: Rc<dyn Material> = Rc::new(Dielectric::new(1.5));
    world.add(Rc::new(Sphere::new(Vector3::new(0.0,1.0,0.0), 1.0, Rc::clone(&material1))));

    let material2: Rc<dyn Material> = Rc::new(Lambertian::new(Vector3::new(0.4,0.2,0.1)));
    world.add(Rc::new(Sphere::new(Vector3::new(-4.0,1.0,0.0),1.0,Rc::clone(&material2))));

    let material3: Rc<dyn Material> = Rc::new(Metal::new(Vector3::new(0.7,0.6,0.5), 0.0));
    world.add(Rc::new(Sphere::new(Vector3::new(4.0,1.0,0.0),1.0, Rc::clone(&material3))));

    world
}

fn main() {

    // Image
    let aspect_ratio = 3.0 / 2.0;
    let image_width = 600;
    let image_height = (image_width as f64 / aspect_ratio) as i64;
    let samples_per_pixel = 50;
    let max_depth = 50;

    // World
    
    /*
    let mut world = HittableList::new();

    let material_ground = (Lambertian::new(vector::Vector3::new(0.8, 0.8, 0.0)));
    let material_center = (Lambertian::new(vector::Vector3::new(0.1, 0.2, 0.5)));
    let material_left: Rc<dyn Material>   = Rc::new(Dielectric::new(1.5));
    let material_right  = (Metal::new(vector::Vector3::new(0.8, 0.6, 0.2),0.0));

    world.add(Rc::new(Sphere::new(vector::Vector3::new(0.0, -100.5, -1.0), 100.0, Rc::new(material_ground))));
    world.add(Rc::new(Sphere::new(vector::Vector3::new(0.0, 0.0, -1.0), 0.5, Rc::new(material_center))));
    world.add(Rc::new(Sphere::new(vector::Vector3::new(-1.0, 0.0, -1.0), 0.5, Rc::clone(&material_left))));
    world.add(Rc::new(Sphere::new(vector::Vector3::new(-1.0, 0.0, -1.0), -0.4, Rc::clone(&material_left))));
    world.add(Rc::new(Sphere::new(vector::Vector3::new(1.0, 0.0, -1.0), 0.5, Rc::new(material_right))));
*/

    let world = random_scene();
    println!("P3\n {} {}", image_width, image_height);

    // Camera
    let lookfrom = Vector3::new(13.0,2.0,3.0);
    let lookat = Vector3::new(0.0,0.0,0.0);
    let vup = Vector3::new(0.0,1.0,0.0);
    let dist_to_focus = 10.0;
    let aperture = 0.1;

    let cam = Camera::new(lookfrom, lookat, vup, 20.0, aspect_ratio, aperture, dist_to_focus);
    
    println!("255");

    for j in ( 0..(image_height-1) ).rev() {

        eprint!("\r{}", j);

        for i in 0..image_width {

            let i_float = i as f64;
            let j_float = j as f64;

            let img_wdth = image_width as f64;
            let img_height = image_height as f64;

            let mut pixel_color = vector::Vector3::new(0.0, 0.0, 0.0);

            // Anti-aliasing
            for s in 0..samples_per_pixel {
                let u = (i_float + rand::thread_rng().gen_range(0.0,1.0)) / (img_wdth -1.0);
                let v = (j_float + rand::thread_rng().gen_range(0.0,1.0)) / (img_height-1.0);

                let r = cam.get_ray(u, v);
                pixel_color = pixel_color + ray_color(&r, &world, max_depth);
            }
            color::write_color(pixel_color, samples_per_pixel as f64);
        }
    }
}
