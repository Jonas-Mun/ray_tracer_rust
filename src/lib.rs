use vector::Vector3;
use crate::ray::Ray;
use std::rc::Rc;
use crate::material::Material;
use rand::Rng;
use libm;


pub type Point3 = vector::Vector3;  // 3D point
pub type Color = vector::Vector3;   // RGB color


pub struct HittableList {
    hittable_list: Vec<Rc<dyn Hittable>>,
}

impl HittableList {
    pub fn new() -> HittableList {
        let hittable_list = Vec::<Rc<dyn Hittable>>::new();

        HittableList { hittable_list }
    }

    pub fn clear(&mut self) {
        self.hittable_list.clear(); 
    }

    pub fn add(&mut self, object: Rc<dyn Hittable>) {
        self.hittable_list.push(Rc::clone(&object)); 
    }

    pub fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {

        let mut closest_so_far = t_max;
        let mut curr_record: Option<HitRecord> = None;

        for object in &self.hittable_list {
            if let Some(record) = object.hit(r, t_min, closest_so_far) {
                closest_so_far = record.t;
                curr_record = Some(record);
            }
        }

        if let Some(x) = curr_record {
            Some(x)
        } else {
            None
        }
    }
}

pub struct HitRecord {
    pub p: Point3,
    pub normal: vector::Vector3,
    pub t: f64,
    pub front_face: bool,
    pub mat_ptr: Rc<dyn Material>,
}

impl HitRecord {
    pub fn new(p: Point3, t: f64, outward_normal: vector::Vector3, ray: &Ray, mat: Rc<dyn Material>) -> HitRecord {
        let (front_face, normal) = HitRecord::set_face_normal(ray, outward_normal);
        HitRecord { p, normal, t, front_face, mat_ptr: Rc::clone(&mat) }
    }

    fn set_face_normal(r: &Ray, outward_normal: vector::Vector3) -> (bool, vector::Vector3) {
        let front_face: bool = vector::Vector3::dot(r.direction(), &outward_normal) < 0.0;

        let normal = if front_face {
            outward_normal
        } else {
            -outward_normal
        };

        (front_face, normal)
    }
}

pub trait Hittable {
    fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord>;

}

pub mod material {
    use crate::HitRecord;
    use vector::Vector3;
    use crate::ray::Ray;
    use num::pow;
    use rand::Rng;

    pub fn schlick(cosine: f64, ref_idx: f64) -> f64 {
        let mut r0 = (1.0-ref_idx) / (1.0+ref_idx);
        r0 = r0 * r0;
        return r0 + (1.0-r0)*pow((1.0-cosine), 5.0 as usize);

    }

    pub trait Material {
         fn scatter(&self, r_in: &Ray, rec: &HitRecord) -> Option<(Ray, Vector3)>;
    }

    /* Materials */
    pub struct Lambertian {
        pub albedo: Vector3,
    }

    impl Lambertian {
        pub fn new(albedo: Vector3) -> Lambertian {
            Lambertian { albedo }
        }
    }

    pub struct Metal {
        pub albedo: Vector3,
        pub fuzz: f64,
    }

    impl Metal {
        pub fn new(albedo: Vector3, fuzz: f64) -> Metal {
            let fuzz = if fuzz > 1.0 {
                1.0
            } else {
                fuzz
            };
            Metal { albedo, fuzz }
        }
    }

    pub struct Dielectric {
        ref_idx: f64,
    }

    impl Dielectric {
        pub fn new(ri: f64) -> Dielectric {
            Dielectric { ref_idx: ri }
        }
    }

    /* Traits */
    impl Material for Lambertian {
        fn scatter(&self, r_in: &Ray, rec: &HitRecord) -> Option<(Ray, Vector3)> {
            let scatter_direction = rec.normal + Vector3::random_unit_vector();
            let scattered = Ray::new(rec.p, scatter_direction);

            Some((scattered, self.albedo))
        }
    }

    impl Material for Metal {
        fn scatter(&self, r_in: &Ray, rec: &HitRecord) -> Option<(Ray, Vector3)> {
            let reflected = Vector3::reflect(Vector3::unit_vector(r_in.direction()), rec.normal);
            let scattered = Ray::new(rec.p, reflected + self.fuzz*Vector3::random_in_unit_sphere());

            if Vector3::dot(scattered.direction(), &rec.normal) > 0.0 {
                Some((scattered, self.albedo))
            } else {
                None
            }

        }
    }

    impl Material for Dielectric {
        fn scatter(&self, r_in: &Ray, rec: &HitRecord) -> Option<(Ray, Vector3)> {
            let etai_over_etat = if rec.front_face {1.0/self.ref_idx } else { self.ref_idx };
            let unit_direction = Vector3::unit_vector(r_in.direction());

            let cos_theta = libm::fmin(Vector3::dot(&-unit_direction, &rec.normal),1.0);
            let sin_theta = (1.0 - cos_theta*cos_theta).sqrt();
            
            let attenuation = Vector3::new(1.0, 1.0,1.0);

            // total internal reflection
            if etai_over_etat * sin_theta > 1.0 {
                let reflected = Vector3::reflect(unit_direction, rec.normal);
                let scattered = Ray::new(rec.p, reflected);

                return Some( (scattered, attenuation) );
            }
            // schlick
            let reflect_prob = schlick(cos_theta, etai_over_etat);
            if rand::thread_rng().gen_range(0.0,1.0) < reflect_prob {
                let reflected = Vector3::reflect(unit_direction, rec.normal);
                let scattered = Ray::new(rec.p, reflected);
                return Some( (scattered, attenuation) );
            }

            let refracted = Vector3::refract(unit_direction, rec.normal, etai_over_etat);
            let scattered = Ray::new(rec.p, refracted);

            Some((scattered, attenuation))
        }
    }
}

pub mod camera {
    use crate::ray::Ray;
    use vector::Vector3;
    use crate::Point3;
    use num::clamp;

    pub struct Camera {
        horizontal: vector::Vector3,
        vertical: vector::Vector3,
        origin: Point3,
        lower_left_corner: Point3,
        u: Vector3,
        v: Vector3,
        w: Vector3,
        lens_radius: f64,
    }

    impl Camera {
        pub fn new(lookfrom: Vector3, 
                   lookat: Vector3, 
                   vup: Vector3, 
                   vfov: f64, 
                   aspect_ratio: f64,
                   aperture: f64,
                   focus_dist: f64) -> Camera {

            let theta = vfov * (std::f64::consts::PI/180.0);
            let h = libm::tan(theta/2.0);
            let viewport_height = 2.0 * h;
            let viewport_width = aspect_ratio * viewport_height;

            let w = Vector3::unit_vector(&(lookfrom - lookat));
            let u = Vector3::unit_vector(&Vector3::cross(&vup, &w));
            let v = Vector3::cross(&w, &u);

            let origin = lookfrom;
            let horizontal = focus_dist * viewport_width * u;
            let vertical = focus_dist * viewport_height * v;
            let lower_left_corner = origin - horizontal/2.0 - vertical/2.0 - focus_dist * w;
            let lens_radius = aperture / 2.0;

            Camera {  horizontal, vertical, origin, lower_left_corner, u, v, w, lens_radius }
        }

        pub fn get_ray(&self, s: f64, t: f64) -> Ray {
            let rd = self.lens_radius * Vector3::random_in_unit_disk();
            let offset = self.u * rd.x + self.v * rd.y;
            Ray::new(self.origin + offset, 
                     self.lower_left_corner + s*self.horizontal + t*self.vertical - self.origin - offset)
        }
    }
}

pub mod sphere {
    use vector::Vector3;
    use crate::Point3;
    use crate::ray::Ray;
    use crate::Hittable;
    use crate::HitRecord;
    use crate::material::Material;
    use std::rc::Rc;

    pub struct Sphere {
        pub center: Point3,
        pub radius: f64,
        pub mat_ptr: Rc<dyn Material>,
    }

    impl Sphere {
        pub fn new(center: Point3, radius: f64, m: Rc<dyn Material>) -> Sphere {
            Sphere { center, radius, mat_ptr: m }
        }
    }

    impl Hittable for Sphere {
        fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
            let oc: Vector3 = r.origin - self.center;
            let a = r.direction().len_squared();
            let half_b = Vector3::dot(&oc, r.direction());
            let c = oc.len_squared() - self.radius*self.radius;
            let discriminant = half_b*half_b - a*c;

            if discriminant > 0.0 {
                let root = discriminant.sqrt();

                let temp_n = (-half_b - root) / a;
                let temp_p = (-half_b + root) / a;

                let mut temp: f64 = 0.0;

                if temp_n < t_max && temp_n > t_min {
                    temp = temp_n;
                    let t = temp;
                    let p = r.at(t);
                    let outward_normal = (p - self.center) / self.radius;

                    let record = HitRecord::new(p, t, outward_normal, r, Rc::clone(&self.mat_ptr));
                    return Some(record);
                }  
                if temp_p < t_max && temp_p > t_min {
                    temp = temp_p;
                    let t = temp;
                    let p = r.at(t);
                    let outward_normal = (p - self.center) / (self.radius);
                    let record = HitRecord::new(p, t, outward_normal, r, Rc::clone(&self.mat_ptr));

                    return Some(record);
                }

            }
            None
        }
    }
}

pub mod ray {
    use crate::Point3;

    pub struct Ray {
        pub origin: Point3,
        pub direction: vector::Vector3
    }

    impl Ray {
        pub fn new(orig: Point3, dir: vector::Vector3) -> Ray {
            Ray { origin: orig, direction: dir } 
        }

        pub fn at(&self, t: f64) -> vector::Vector3 {
            self.origin + (self.direction * t)
        }

        pub fn direction(&self) -> &vector::Vector3 {
            &self.direction
        }
    }
}

pub mod color {
    pub use crate::Color;
    use num::clamp;

    pub fn write_color(pixel_color: Color, samples_per_pixel: f64) {
        let constant = 1.0;

        let mut r = (constant * pixel_color.x) as f64;
        let mut g = (constant * pixel_color.y) as f64;
        let mut b = (constant * pixel_color.z) as f64;

        let scale = 1.0 / samples_per_pixel;
        r = (scale * r).sqrt();
        g = (scale * g).sqrt();
        b = (scale * b).sqrt();

        println!("{} {} {}", 256.0 * clamp(r,0.0, 0.999), 256.0 * clamp(g, 0.0, 0.999), 256.0 * clamp(b, 0.0, 0.999));
    }
}
