use std::ops::{Add, Sub, Mul, Div};
use std::ops::Neg;
use rand::Rng;
use libm;

#[derive(Copy, Clone)]
pub struct Vector3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector3 {
    pub fn len_squared(&self) -> f64 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn len(&self) -> f64 {
       self.len_squared().sqrt() 
    }
}

impl Vector3 {
    pub fn new(x: f64, y: f64, z: f64) -> Vector3 {
        Vector3 { x, y, z }
    }

    pub fn dot(u: &Vector3, v: &Vector3) -> f64 {
        u.x * v.x + u.y * v.y + u.z * v.z
    }

    pub fn cross(u: &Vector3, v: &Vector3) -> Vector3 {
        Vector3 {
            x: u.y * v.z - u.z * v.y,
            y: u.z * v.x - u.x * v.z,
            z: u.x * v.y - u.y * v.x,
        }
    }

    pub fn unit_vector(v: &Vector3) -> Vector3 {
        *v / v.len()
    }

    pub fn random() -> Vector3 {
        let x = rand::thread_rng().gen_range(0.0,1.0);
        let y = rand::thread_rng().gen_range(0.0, 1.0);
        let z = rand::thread_rng().gen_range(0.0, 1.0);
        Vector3::new(x, y, z)
    }

    pub fn random_spec(min: f64, max: f64) -> Vector3 {
        let x = rand::thread_rng().gen_range(min, max);
        let y = rand::thread_rng().gen_range(min, max);
        let z = rand::thread_rng().gen_range(min, max);

        Vector3::new(x, y, z)
    }

    pub fn random_in_unit_sphere() -> Vector3 {
        loop {
            let p = Vector3::random_spec(-1.0, 1.0);
            if p.len_squared() >= 1.0 {
                continue
            }
            return p;
        }
    }

    pub fn random_unit_vector() -> Vector3 {
        let a = rand::thread_rng().gen_range(0.0, 2.0 * std::f64::consts::PI);
        let z = rand::thread_rng().gen_range(-1.0, 1.0);
        let r: f64 = (1.0 - z*z);
        let r = r.sqrt();

        Vector3::new(r * f64::cos(a), r * f64::sin(a), z)
    }

    pub fn random_in_hemisphere(normal: &Vector3) -> Vector3 {
        let unit_in_sphere = Vector3::random_in_unit_sphere();
        if Vector3::dot(&unit_in_sphere, &normal) > 0.0 {
            return unit_in_sphere;
        }
        return -unit_in_sphere;
    }

    pub fn reflect(v: Vector3, n: Vector3) -> Vector3 {
        v - 2.0*Vector3::dot(&v,&n)*n
    }

    pub fn refract(uv: Vector3, n: Vector3, etai_over_etat: f64) -> Vector3 {
        let cos_theta = Vector3::dot(&-uv, &n);
        let r_out_perp = etai_over_etat * (uv + cos_theta*n);
        let r_out_parallel = -libm::fabs(1.0 - r_out_perp.len_squared()).sqrt() * n;

        return r_out_perp + r_out_parallel;
    }

    pub fn random_in_unit_disk() -> Vector3 {
        loop {
            let p = Vector3::new(rand::thread_rng().gen_range(-1.0,1.0), rand::thread_rng().gen_range(-1.0,1.0),0.0);
            if (p.len_squared() >= 1.0) {
                continue
            }

            return p;
        }
    }
}


impl Add for Vector3 {
    type Output = Vector3;

    fn add(self, other: Vector3) -> Vector3 {
        Vector3 { x: self.x + other.x, y: self.y + other.y, z: self.z + other.z }
    }
}

impl Sub for Vector3 {
    type Output = Vector3;

    fn sub(self, other: Vector3) -> Vector3 {
        Vector3 {x: self.x - other.x, y: self.y - other.y, z: self.z - other.z }
    }

}

impl Mul<f64> for Vector3 {
    type Output = Vector3;

    fn mul(self, other: f64) -> Vector3 {
        Vector3 {x: self.x * other, y: self.y * other, z: self.z * other }
    }
}

impl Mul<Vector3> for f64 {
    type Output = Vector3;

    fn mul(self, other: Vector3) -> Vector3 {
        Vector3 { x: self * other.x, y: self * other.y, z: self * other.z }
    }
}

impl Mul<Vector3> for Vector3 {
    type Output = Vector3;

    fn mul(self, other: Vector3) -> Vector3 {
        Vector3 { x: self.x * other.x, y: self.y * other.y, z: self.z * other.z }
    }
}

impl Div<f64> for Vector3 {
    type Output = Vector3;

    fn div(self, other: f64) -> Vector3 {
        self * (1.0/other)
    }
}

impl Neg for Vector3 {
    type Output = Vector3;

    fn neg(self) -> Vector3 {
        Vector3 { x: -self.x , y: -self.y, z: -self.z }
    }
}

